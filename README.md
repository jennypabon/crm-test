# CRM Upplication #

Upplication Content Repository Management called Fenix

### Features ###

* Admin secure login
* Admin sales page
* ChildUpp search page
* WebUser search page with childupp resume per WebUser
* Defaulters page
* Market Requests page

### Roadmap ###

* Manage market's publication: update state (approved, rejected, waiting for markets, none yet), search new publish requests, order by priority (olders first)
* Study: downgrade, upgrade, stats...

### How do I get set up? ###

Install local:

```gradlew clean build```

Run local:

```gradlew clean bootRun```

The app is deployed at http://localhost:8080/

Run local with docker and with profile 'dev':

```gradle clean build```
```docker build -t upplication/fenix .```
```docker run -e "SPRING_OPTS=-agentlib:jdwp=transport=dt_socket,address=5005,server=y,suspend=n" -e "SPRING_PROFILES_ACTIVE=dev" -p 8080:8080 -p 5005:5005 --name fenix  upplication/fenix```

All the configuration of the app are setted in the yml file: resources/application-{profile}.yml.
If you want to change the development configuration you must change the settings in the file application-dev.yml
If you want to change the production configuration you must change the settings in the file application.yml
The shared properties are stored in application.yml

#### Profiles

There are three profiles, default (production), develop and test.

- default: with production properties (bbdd, apis endpoints...)
- develop: with develop properties (bbdd, apis endpoints...)
- test: only for testing

At application.properties is possible to change the rabbitMQ Server parameters.

Build Jar:

Generate the jar at /target folder with

```
gradlew clean build
```

Then is possible to run this jar with specific parameters the command

```
java -jar -Dspring.profiles.active=ACTIVE_PROFILE fenix-VERSION.jar
```

The app is deployed at http://localhost:8080/
