package com.upplication.crm.webusers;

import es.upplication.entities.type.ChildUppRequestType;

import java.util.Date;
import java.util.List;

public class ChildUppInfo {

    private List<ChildUppLine> childUpps;
    private List<ChildUppRequestLine> requests;

    public List<ChildUppLine> getChildUpps() {
        return childUpps;
    }

    public void setChildUpps(List<ChildUppLine> childUpps) {
        this.childUpps = childUpps;
    }

    public List<ChildUppRequestLine> getRequests() {
        return requests;
    }

    public void setRequests(List<ChildUppRequestLine> requests) {
        this.requests = requests;
    }

    public static class ChildUppLine {
        private int id;
        private Date created;
        private String name;
        private String pricingPlan;
        private Date deleted;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPricingPlan() {
            return pricingPlan;
        }

        public void setPricingPlan(String pricingPlan) {
            this.pricingPlan = pricingPlan;
        }

        public Date getCreated() {
            return created;
        }

        public void setCreated(Date created) {
            this.created = created;
        }

        public Date getDeleted() {
            return deleted;
        }

        public void setDeleted(Date deleted) {
            this.deleted = deleted;
        }
    }

    public static class ChildUppRequestLine {

        private int id;
        private Date created;
        private String pricingPlan;
        private Date resolved;
        private ChildUppRequestType type;

        public ChildUppRequestType getType() {
            return type;
        }

        public void setType(ChildUppRequestType type) {
            this.type = type;
        }

        public String getPricingPlan() {
            return pricingPlan;
        }

        public void setPricingPlan(String pricingPlan) {
            this.pricingPlan = pricingPlan;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public Date getCreated() {
            return created;
        }

        public void setCreated(Date created) {
            this.created = created;
        }

        public Date getResolved() {
            return resolved;
        }

        public void setResolved(Date resolved) {
            this.resolved = resolved;
        }
    }

}
