package com.upplication.crm.webusers;

import com.upplication.crm.repository.ChildUppRequestRepository;
import com.upplication.crm.repository.WebUserRepository;
import es.upplication.entities.ChildUpp;
import es.upplication.entities.ChildUppRequest;
import es.upplication.entities.type.CustomerType;
import es.upplication.security.TokenAdminLogin;
import es.upplication.security.TokenLogin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static com.upplication.crm.repository.WebUserSpecification.matchesCriteria;
import static com.upplication.crm.repository.ChildUppRequestSpecification.*;
import static org.springframework.data.jpa.domain.Specifications.where;

@Service
public class WebUsersManager {

    @Autowired
    WebUserRepository webUserRepository;
    @Autowired
    ChildUppRequestRepository childUppRequestRepository;
    @Autowired
    TokenLogin tokenAplicatecaLogin;
    @Autowired
    TokenAdminLogin tokenAdminLogin;
    @Value("${upplication.dashboard.url}")
    String dashboardUrl;

    @Transactional(readOnly = true)
    public Page<WebUser> getWebUsers(Pageable pageable, String email, String customerId, CustomerType customerType, Integer childUppId, boolean deleted) {
        Page<es.upplication.entities.WebUser> webUserPage = webUserRepository.findAll(matchesCriteria(email, customerId, customerType, childUppId, deleted), pageable);
        List<es.upplication.entities.WebUser> webUsers = webUserPage.getContent();
        return new PageImpl<>(populate(webUsers), pageable, webUserPage.getTotalElements());
    }


    @Transactional(readOnly = true)
    public ChildUppInfo getChildUppInfo(int idWebUser) {
        ChildUppInfo result = new ChildUppInfo();
        result.setChildUpps(new ArrayList<>());
        result.setRequests(new ArrayList<>());
        es.upplication.entities.WebUser webUser = webUserRepository.findOne(idWebUser);
        for (ChildUpp childUpp : webUser.getChildUPPs()) {
            ChildUppInfo.ChildUppLine line = new ChildUppInfo.ChildUppLine();
            line.setId(childUpp.getId());
            line.setCreated(childUpp.getCreationDate());
            line.setName(childUpp.getName());
            line.setDeleted(childUpp.getDeleteDate());
            line.setPricingPlan(childUpp.getFeatures().getNamePricing());
            result.getChildUpps().add(line);
        }

        List<ChildUppRequest> requests =  childUppRequestRepository.findAll(where(filterByWebUser(idWebUser)));

        for (ChildUppRequest request : requests) {
            ChildUppInfo.ChildUppRequestLine line = new ChildUppInfo.ChildUppRequestLine();
            line.setCreated(request.getCreated());
            line.setId(request.getId());
            line.setPricingPlan(request.getPricing().getName());
            line.setResolved(request.getResolved());
            line.setType(request.getType());
            result.getRequests().add(line);
        }

        return result;
    }

    private List<WebUser> populate(List<es.upplication.entities.WebUser> webUsers) {
        List<WebUser> result = new ArrayList<>();

        String token = tokenAdminLogin.create(TokenAdminLogin.PASSPHRASE);
        String type = tokenAdminLogin.create("1");

        for (es.upplication.entities.WebUser webUser : webUsers) {
            String loginUrl;

            switch (webUser.getAccount().getOrganization().getName()) {
                case "upplication": {
                    String field = tokenAdminLogin.create(webUser.getEmail());
                    if (webUser.getCustomerType() == CustomerType.UPPLICATION) {
                        loginUrl = "https://dashboard.upplication.io/v2/admin/login?token=" + token + "&field=" + field + "&type=" + type;
                    } else {
                        // Aplicateca use the same domain and dashboard but the old version.
                        // if the domain will be different (like a another product like wingu) then the organization must NOT be upplication
                        loginUrl = "https://dashboard.upplication.io/v2/aplicateca/login?token=" + tokenAplicatecaLogin.create(webUser.getCustomerId());
                    }
                    break;
                }
                case "wingu":
                case "vodafone": {
                    String field = webUser.getEmail() != null ? tokenAdminLogin.create(webUser.getEmail()) : null;
                    loginUrl = "https://dashboard.winguapps.io/v2/admin/login?token=" + token + "&field=" + field + "&type=" + type;
                    break;
                }
                default: {
                    String field = tokenAdminLogin.create(webUser.getEmail());
                    loginUrl = "https://dashboard.upplication.io/web/admin/login?token=" + token + "&field=" + field + "&type=" + type;
                    break;
                }
            }

            result.add(WebUser.fromEntity(webUser, loginUrl));
        }

        return result;
    }
}
