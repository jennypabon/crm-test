package com.upplication.crm.sales;

import javax.validation.constraints.Digits;

import com.upplication.crm.common.forms.SearchAdminForm;

public class SalesAdminForm extends SearchAdminForm {
    private Integer childUppId;

    @Digits(integer = 15, fraction = 4)
    public Integer getChildUppId() {
        return childUppId;
    }

    public void setChildUppId(Integer childUppId) {
        this.childUppId = childUppId;
    }

}
