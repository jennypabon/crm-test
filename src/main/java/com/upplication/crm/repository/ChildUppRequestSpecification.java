package com.upplication.crm.repository;

import es.upplication.entities.ChildUppRequest;
import es.upplication.entities.WebUser;
import es.upplication.entities.type.ChildUppRequestType;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.Date;

public class ChildUppRequestSpecification {

    public static Specification<ChildUppRequest> filterByWebUser(final int webUser) {
        return (childUppRequest, query, cb) -> {
            Join<ChildUppRequest, WebUser> webuser = childUppRequest.join("webUser");
            return cb.equal(webuser.get("id"), webUser);
        };
    }

    public static Specification<ChildUppRequest> filterByRequestType(final ChildUppRequestType type) {
        return (childUppRequest, query, cb) -> cb.equal(childUppRequest.get("type"), type);
    }

    public static Specification<ChildUppRequest> filterByCreated(final Date start, Date end) {
        return (childUppRequest, query, cb) -> cb.and(cb.greaterThanOrEqualTo(childUppRequest.get("created"), start), cb.lessThanOrEqualTo(childUppRequest.get("created"), end));
    }

    public static Specification<ChildUppRequest> filterByNotResolved() {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.isNull(root.get("resolved"));
    }

    public static Specification<ChildUppRequest> filterByResolvedByChildUpp() {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.isNotNull(root.get("childupp"));
    }

    public static Specification<ChildUppRequest> filterByResolvedByOtherChildUppRequest() {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.isNotNull(root.get("childUppRequest"));
    }

    public static Specification<ChildUppRequest> filterByResolvedByOtherChildUppRequest(int idRequest) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.and(criteriaBuilder.isNotNull(root.get("childUppRequest")), criteriaBuilder.equal(root.get("childUppRequest").get("id"), idRequest));
    }

}
