package com.upplication.crm.repository;

import es.upplication.entities.HistoricFeatures;
import es.upplication.entities.PaymentOrder;
import es.upplication.entities.type.HistoricFeaturesType;
import es.upplication.entities.type.StatusHistoricFeatures;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class FeaturesSpecification {

    public static Specification<HistoricFeatures> filterByPaymentOrder(final int paymentOrder) {
        return (root, query, cb) -> {
            // subquery
            Subquery<Integer> subquery = query.subquery(Integer.class);
            Root<PaymentOrder> innerPaymentOrder = subquery.from(PaymentOrder.class);

            subquery
                    .select(innerPaymentOrder.<Integer>get("id"))
                    .where(
                            cb.equal(innerPaymentOrder.<Integer>get("feature"), root.<Integer>get("id"))
                    );
            return cb.and(cb.or(cb.equal(root.get("type"), HistoricFeaturesType.ACTIVE), cb.equal(root.get("type"), HistoricFeaturesType.HISTORY)), cb.exists(subquery));
        };
    }

    public static Specification<HistoricFeatures> filterByChildUppId(final int childUppId) {
        return (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();

            if (childUppId > 0) {
                predicates.add(criteriaBuilder.equal(root.<Integer>get("childUpp"), childUppId));
            }

            predicates.add(criteriaBuilder.equal(root.<HistoricFeatures>get("type"), HistoricFeaturesType.ACTIVE));

            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }

    public static Specification<HistoricFeatures> betweenCreated(final Date start, Date end) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.and(criteriaBuilder.greaterThanOrEqualTo(root.get("created"), start), criteriaBuilder.lessThanOrEqualTo(root.get("created"), end));
    }

    public static Specification<HistoricFeatures> betweenDeleted(final Date start, Date end) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.and(criteriaBuilder.greaterThanOrEqualTo(root.get("deleted"), start), criteriaBuilder.lessThanOrEqualTo(root.get("deleted"), end));
    }

    public static Specification<HistoricFeatures> byType(HistoricFeaturesType type) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.<HistoricFeaturesType>get("type"), type);
    }

    public static Specification<HistoricFeatures> notStatus(StatusHistoricFeatures... status) {
        return (root, criteriaQuery, criteriaBuilder) -> {
            Predicate[] nots = Arrays.stream(status).map((s) -> criteriaBuilder.notEqual(root.<StatusHistoricFeatures>get("status"), s)).toArray(Predicate[]::new);
            return criteriaBuilder.and(nots);
        };
    }
}