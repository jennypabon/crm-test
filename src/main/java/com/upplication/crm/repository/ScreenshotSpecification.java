package com.upplication.crm.repository;


import es.upplication.entities.Screenshot;
import es.upplication.entities.type.ScreenshotDeviceType;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

public class ScreenshotSpecification {

    public static Specification<Screenshot> matchesCriteria(final int requestId, final ScreenshotDeviceType deviceType) {
        return (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();

            if (requestId > 0) {
                predicates.add(criteriaBuilder.equal(root.<Integer>get("requestId"), requestId));
            }

            if (deviceType != null){
                predicates.add(criteriaBuilder.equal(root.<ScreenshotDeviceType>get("deviceType"), deviceType));
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }

    public static Specification<Screenshot> filterByRequestId(final int requestId) {
        return (screenshot, query, cb) -> cb.equal(screenshot.<Integer>get("requestId"), requestId);
    }

    public static Specification<Screenshot> matchesCriteria(final int requestId, final int position, final ScreenshotDeviceType deviceType) {
        return (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();

            if (requestId > 0) {
                predicates.add(criteriaBuilder.equal(root.<Integer>get("requestId"), requestId));
            }

            if (position > 0) {
                predicates.add(criteriaBuilder.equal(root.<Integer>get("position"), position));
            }

            if (deviceType != null){
                predicates.add(criteriaBuilder.equal(root.<ScreenshotDeviceType>get("deviceType"), deviceType));
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }

}
