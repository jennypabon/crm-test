package com.upplication.crm.repository;


import es.upplication.entities.ChildUpp;
import es.upplication.entities.ChildUppOS;
import es.upplication.entities.ChildUppOSId;
import es.upplication.entities.OS;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

public class ChildUppOsSpecification {

    public static Specification<ChildUppOS> filterByBundleId(final String bundleId) {
        return (childUppOs, query, cb) -> cb.equal(childUppOs.get("bundleId"), bundleId);
    }

    public static Specification<ChildUppOS> filterByChildUppIdAndOs(final int childUppId, final int soId) {
        return (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();

            predicates.add(criteriaBuilder.equal(root.<ChildUppOSId>get("pk").<ChildUpp>get("childUpp").<Integer>get("id"), childUppId));
            predicates.add(criteriaBuilder.equal(root.<ChildUppOSId>get("pk").<OS>get("os").<Integer>get("id"), soId));

            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }

}
