package com.upplication.crm.repository;

import es.upplication.entities.PaymentOrder;
import es.upplication.entities.WebUser;
import es.upplication.entities.type.StatusPaymentOrderType;
import org.joda.time.LocalDateTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public interface PaymentOrderRepository extends JpaRepository<PaymentOrder, Integer>,
        JpaSpecificationExecutor<PaymentOrder> {
}
