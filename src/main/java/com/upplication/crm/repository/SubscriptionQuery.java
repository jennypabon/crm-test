package com.upplication.crm.repository;

import com.upplication.crm.repository.dto.Subscription;

import java.math.BigInteger;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;

public interface SubscriptionQuery {

    String getPricingName();
    String getDate();
    BigInteger getCount();


    DateTimeFormatter formatter = new DateTimeFormatterBuilder()
            .appendPattern("yyyy-MM")
            .parseDefaulting(ChronoField.DAY_OF_MONTH, 1)
            .toFormatter();

    default Subscription toSubscription() {
        Subscription r = new Subscription();
        r.setPricingName(getPricingName());
        // all dates at start of the month to allow group by month
        r.setDate(LocalDate.parse(getDate(), formatter));
        r.setCount(getCount().intValue());
        return r;
    }


}
