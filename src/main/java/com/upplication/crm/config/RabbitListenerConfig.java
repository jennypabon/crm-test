package com.upplication.crm.config;

import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class RabbitListenerConfig implements RabbitListenerConfigurer {

    private final Environment env;

    public RabbitListenerConfig(final Environment env) {
        this.env = env;
    }

    /**
     * Create a new connection factory to RabbitMQ with the values at properties file
     * @return ConnectionFactory Connection factory with custom values
     */
    @Bean
    public ConnectionFactory rabbitConnectionFactory() {
        String host = env.getProperty("rabbitmq.host");
        int port = Integer.parseInt(env.getProperty("rabbitmq.port"));
        String username = env.getProperty("rabbitmq.username");
        String password = env.getProperty("rabbitmq.password");

        CachingConnectionFactory factory = new CachingConnectionFactory(
                host,
                port
        );
        factory.setUsername(username);
        factory.setPassword(password);

        return factory;
    }

    /**
     * Listener for RabbitMq with setting to control the Manual ACK, and maximum customers at this time
     * @return SimpleRabbitListenerContainerFactory
     */
    @Bean
    public SimpleRabbitListenerContainerFactory myRabbitListenerContainerFactory() {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setConnectionFactory(rabbitConnectionFactory());
        factory.setMaxConcurrentConsumers(1);
        factory.setMessageConverter(producerJackson2MessageConverter());
        factory.setAcknowledgeMode(AcknowledgeMode.MANUAL);
        return factory;
    }

    @Override
    public void configureRabbitListeners(final RabbitListenerEndpointRegistrar registrar) {
        registrar.setContainerFactory(myRabbitListenerContainerFactory());
    }

    @Bean
    public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public RabbitTemplate rabbitTemplate() {
        RabbitTemplate template = new RabbitTemplate(rabbitConnectionFactory());
        template.setMessageConverter(producerJackson2MessageConverter());
        return template;
    }

}
