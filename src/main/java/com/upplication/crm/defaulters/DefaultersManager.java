package com.upplication.crm.defaulters;

import com.upplication.crm.repository.ChildUppRepository;
import com.upplication.crm.support.web.HttpUtil;
import es.upplication.entities.ChildUpp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class DefaultersManager {

    @Autowired
    private ChildUppRepository childUppRepository;
    
    @Value("${upplication.childupp-api.core-url}")
    private String dashboardApiUrl;

    @Transactional
    public List<DefaulterLine> getDefaulters() {
        List<DefaulterLine> lines = new ArrayList<>();
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(HttpUtil.MEDIA_TYPE_APPLICATION_JSON));
        HttpEntity<Debts> entity = new HttpEntity<>(headers);
        Debts debts = restTemplate.exchange(dashboardApiUrl + "billing/debts", HttpMethod.GET, entity, Debts.class).getBody();

        for (DebtLine debt : debts.getDebts()) {
            ChildUpp childUpp = childUppRepository.findOne(debt.getChildUpp());
            
            DefaulterLine line = new DefaulterLine();
            line.setCreator(childUpp.getOwner().getName() + " " + childUpp.getOwner().getSurname());
            line.setEmail(childUpp.getOwner().getEmail());
            line.setChildUppName(childUpp.getName());
            line.setChildUppId(childUpp.getId());
            line.setDate(debt.getBillingDate());
            line.setQuantity(debt.getAmount());
            line.setType(debt.getType());
            line.setStatus(childUpp.getFeatures().getStatus().toString());
            line.setType(debt.getType());
            
            lines.add(line);
        }

        return lines;
    }


}
