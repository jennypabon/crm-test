package com.upplication.crm.market.requests;

import com.upplication.crm.config.util.security.SecurityPassword;
import com.upplication.crm.market.requests.domain.*;
import com.upplication.crm.market.requests.email.PublishRequestEmail;
import com.upplication.crm.market.requests.queue.QueueSender;
import com.upplication.crm.market.requests.s3.AwsS3Service;
import com.upplication.crm.market.requests.util.BuilderMessageUtil;
import com.upplication.crm.market.requests.util.CreationStatusUtil;
import com.upplication.crm.repository.*;
import com.upplication.s3fs.S3Path;
import es.upplication.entities.*;
import es.upplication.entities.type.*;
import es.upplication.type.OperatingSystemType;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

import static com.upplication.crm.repository.ChildUppOsSpecification.filterByBundleId;
import static com.upplication.crm.repository.MarketRequestSpecification.byChildUppId;
import static com.upplication.crm.repository.MarketRequestSpecification.groupByChildUppId;
import static org.springframework.data.jpa.domain.Specifications.where;
import static com.upplication.crm.repository.ScreenshotSpecification.matchesCriteria;

@Service
public class RequestsManager {

    Logger logger = LoggerFactory.getLogger(RequestsManager.class);

    private final MarketRequestRepository marketRequestRepository;
    private final PublishRequestEmail publishRequestEmail;
    private final ChildUppOsRepository childUppOsRepository;
    private final ChildUppRepository childUppRepository;
    private final ScreenshotRepository screenshotRepository;
    private final QueueSender queueSender;
    private final AppleSignRepository appleSignRepository;
    private final CreationStatusUtil creationStatusUtil;
    private final GooglePlaySignRepository googlePlaySignRepository;
    private final BuilderMessageUtil builderMessageUtil;
    private final AwsS3Service awsS3Service;

    public RequestsManager(final MarketRequestRepository marketRequestRepository, final PublishRequestEmail publishRequestEmail,
                           final ChildUppOsRepository childUppOsRepository, final ChildUppRepository childUppRepository,
                           final ScreenshotRepository screenshotRepository, final QueueSender queueSender,
                           final AppleSignRepository appleSignRepository, final CreationStatusUtil creationStatusUtil,
                           final GooglePlaySignRepository googlePlaySignRepository, final BuilderMessageUtil builderMessageUtil,
                           final AwsS3Service awsS3Service) {
        this.marketRequestRepository = marketRequestRepository;
        this.publishRequestEmail = publishRequestEmail;
        this.childUppOsRepository = childUppOsRepository;
        this.childUppRepository = childUppRepository;
        this.screenshotRepository = screenshotRepository;
        this.queueSender = queueSender;
        this.appleSignRepository = appleSignRepository;
        this.creationStatusUtil = creationStatusUtil;
        this.googlePlaySignRepository = googlePlaySignRepository;
        this.builderMessageUtil = builderMessageUtil;
        this.awsS3Service = awsS3Service;
    }

    /**
     * get Market Requests pageable
     *
     * @param pageable Pageable
     * @return Request Page
     */
    @Transactional
    public Page<Request> getMarketRequests(Pageable pageable, int childUppId) {
        // load data
        Page<PublishRequest> requestsPage = marketRequestRepository.findAll(where(byChildUppId(childUppId)).and(groupByChildUppId()), pageable);
        List<PublishRequest> requests = requestsPage.getContent();
        return new PageImpl<>(populate(requests), pageable, requestsPage.getTotalElements());
    }

    private List<Request> populate(List<PublishRequest> requests) {
        List<Request> results = new ArrayList<>();
        for (PublishRequest publishRequest : requests) {
            Request request = new Request();
            request.setId(publishRequest.getId());
            request.setChildUppId(publishRequest.getChildUpp().getId());
            if (!results.contains(request)) {
                request.setDate(publishRequest.getRequestDate());
                request.setChildUppName(publishRequest.getChildUpp().getName());
                request.setOrganizationName(publishRequest.getChildUpp().getOwner().getAccount().getOrganization().getName());
                for (ChildUppOS os : publishRequest.getChildUpp().getOs()) {
                    if (os.getOS().getId() == OperatingSystemType.ANDROID.getValue()) {
                        request.setApkUrl(os.getUrl());
                        request.setAndroidVersion(os.getVersion());
                        request.setAndroidMarketUrl(os.getMarketUrl());
                        request.setAndroidBundleId(os.getBundleId());
                        request.setAndroidPublicationDate(os.getPublicationDate() != null ? os.getPublicationDate().toString() : null);
                    } else if (os.getOS().getId() == OperatingSystemType.IOS.getValue()) {
                        request.setIpaUrl(os.getUrl());
                        request.setIosVersion(os.getVersion());
                        request.setIosMarketUrl(os.getMarketUrl());
                        request.setIosBundleId(os.getBundleId());
                        request.setIosPublicationDate(os.getPublicationDate() != null ? os.getPublicationDate().toString() : null);
                    }
                }

                // android sign info
                request.setAndroidPublished(publishRequest.getAndroidStatus().equals(StatusPublishRequest.PUBLISHED));
                request.setOwnAndroidSign(publishRequest.getAndroidSignType() != null && publishRequest.getAndroidSignType() == SignType.CUSTOM);
                GooglePlaySign googlePlaySign = marketRequestRepository.findOne(request.getId()).getGooglePlaySign();
                PublisherAndroid publisherAndroid = new PublisherAndroid();
                if (googlePlaySign != null) {
                    publisherAndroid.setSignatureKeyAlias(googlePlaySign.getSignatureKeyAlias());
                    publisherAndroid.setSignaturePasswordAlias(googlePlaySign.getSignaturePasswordAlias());
                    publisherAndroid.setSignaturePasswordStore(googlePlaySign.getSignaturePasswordStore());
                    publisherAndroid.setSignatureStoreUri(googlePlaySign.getSignatureStoreUri());
                    publisherAndroid.setEmail(googlePlaySign.getEmail());
                    publisherAndroid.setTeamName(googlePlaySign.getTeamName());
                }
                request.setPublisherAndroid(publisherAndroid);

                // iOS sign info
                request.setIosPublished(publishRequest.getIosStatus().equals(StatusPublishRequest.PUBLISHED));
                request.setOwnIosSign(publishRequest.getIosSignType() != null && publishRequest.getIosSignType() == SignType.CUSTOM);
                AppleSign appleSign = marketRequestRepository.findOne(request.getId()).getAppleSign();
                PublisherIos publisherIos = new PublisherIos();
                if (appleSign != null) {
                    publisherIos.setUsername(appleSign.getUsername());
                    publisherIos.setPassword(SecurityPassword.decrypt(appleSign.getPassword()));
                    publisherIos.setTeamId(appleSign.getTeamId());
                    publisherIos.setTeamName(appleSign.getTeamName());
                    publisherIos.setCompanyName(appleSign.getCompanyName());
                    publisherIos.setApplicationPassword(appleSign.getApplicationPassword());
                    publisherIos.setCertificateUrl(appleSign.getCertificateUrl());
                    publisherIos.setCertificatePassword(SecurityPassword.decrypt(appleSign.getCertificatePsw()));
                    publisherIos.setProvisionUrl(appleSign.getProvisionUrl());
                    publisherIos.setAuthKeyId(appleSign.getAuthKeyId());
                    publisherIos.setAuthKeyContent(appleSign.getAuthKeyContent());
                }
                request.setPublisherIos(publisherIos);

                request.setInfo(publishRequest);
                List<Screenshot> mobileScreenshots = screenshotRepository.findAll(matchesCriteria(publishRequest.getId(), ScreenshotDeviceType.iphone6Plus), new Sort(Sort.Direction.ASC, "position"));
                List<Screenshot> tabletScreenshots = screenshotRepository.findAll(matchesCriteria(publishRequest.getId(), ScreenshotDeviceType.ipadPro), new Sort(Sort.Direction.ASC, "position"));
                request.setMobileScreenshots(mobileScreenshots);
                request.setTabletScreenshots(tabletScreenshots);
                results.add(request);
            }
        }

        return results;
    }

    /**
     * Notifies the creator of the app about the publication of the app
     *
     * @param form Publish form
     * @return PublishRequestResponse with the result and a message
     */
    @Transactional
    public PublishRequestResponse savePublication(PublishRequestForm form) {

        PublishRequestResponse result = new PublishRequestResponse();

        String error = "";

        if (form.getIosStatus() == StatusPublishRequest.PUBLISHED && StringUtils.isEmpty(form.getIosMarketUrl())) {
            error += "IOS Status cant be 'PUBLISHED' without the iOS market Url\n";
        }

        if (form.getAndroidStatus() == StatusPublishRequest.PUBLISHED && StringUtils.isEmpty(form.getAndroidMarketUrl())) {
            error += "Android Status cant be 'PUBLISHED' without the Android market Url";
        }

        if (!StringUtils.isEmpty(error)) {
            result.setSuccess(false);
            result.setError(error);
            return result;
        }


        PublishRequest publishRequest = marketRequestRepository.findOne(form.getRequestId());

        publishRequest.setDescription(form.getDescription());
        publishRequest.setKeywords(form.getKeywords());
        publishRequest.setVideoPath(form.getVideoPath());

        publishRequest.setChanges(form.getChanges());
        publishRequest.setPluginsExtra(form.getPluginsExtra());

        Set<ChildUppOS> osSet = publishRequest.getChildUpp().getOs();

        osSet.forEach(os -> {
            String osName = os.getOS().getNameOS().toLowerCase();
            if (osName.equals("android")) {
                os.setMarketUrl(form.getAndroidMarketUrl());
                // send publish email, if the current status is published but previously has another status
                if (form.getAndroidStatus().equals(StatusPublishRequest.PUBLISHED) && !publishRequest.getAndroidStatus().equals(StatusPublishRequest.PUBLISHED)) {
                    os.setPublicationDate(LocalDate.now().toDate());
                    notifyCreatorAppPublished(form, "android");
                }
            } else if (osName.equals("ios")) {
                os.setMarketUrl(form.getIosMarketUrl());
                // send publish email, if the current status is published but previously has another status
                if (form.getIosStatus().equals(StatusPublishRequest.PUBLISHED) && !publishRequest.getIosStatus().equals(StatusPublishRequest.PUBLISHED)) {
                    os.setPublicationDate(LocalDate.now().toDate());
                    notifyCreatorAppPublished(form, "ios");
                }
            }
            childUppOsRepository.save(os);
        });

        publishRequest.setIosStatus(form.getIosStatus());
        publishRequest.setAndroidStatus(form.getAndroidStatus());
        publishRequest.setIpaUrl(form.getIpaUrl());
        publishRequest.setApkUrl(form.getApkUrl());

        publishRequest.setSupportUrl(form.getSupportUrl());
        publishRequest.setContent(form.getContent());
        publishRequest.setCategory(form.getCategory());

        publishRequest.setCopyright(form.getCopyright());
        publishRequest.setContactName(form.getContactName());
        publishRequest.setContactSurname(form.getContactSurname());
        publishRequest.setContactPhone(form.getContactPhone());
        publishRequest.setContactEmail(form.getContactEmail());

        marketRequestRepository.save(publishRequest);

        // save Apple Sign if edit mode is selected
        if (form.isEditAppleStoreAccount()) {

            int childUppId = publishRequest.getChildUpp().getId();

            if (!((!StringUtils.isEmpty(form.getApplePushAuthKeyId()) && !StringUtils.isEmpty(form.getApplePushAuthKeyContent())) ||
                    (StringUtils.isEmpty(form.getApplePushAuthKeyId()) && StringUtils.isEmpty(form.getApplePushAuthKeyContent())))) {
                result.setError("If the AuthKeyId or AuthKeyContent is set then both fields are mandatory for Request ID: " + form.getRequestId());
                result.setSuccess(false);
                return result;
            }

            boolean haveNewMobileProvision = form.getAppleSignMobileProvision() != null && !form.getAppleSignMobileProvision().isEmpty();
            boolean haveNewSignCertificate = form.getAppleSignCertificate() != null && !form.getAppleSignCertificate().isEmpty();

            boolean haveSignCertificate = haveNewSignCertificate || !StringUtils.isEmpty(form.getAppleSignCertificateUrl());
            boolean haveMobileProvision = haveNewMobileProvision || !StringUtils.isEmpty(form.getAppleSignProvisionUrl());
            boolean haveAuthKey = !StringUtils.isEmpty(form.getApplePushAuthKeyId()) && !StringUtils.isEmpty(form.getApplePushAuthKeyContent());

            if (!((!haveAuthKey && !haveMobileProvision && !haveSignCertificate) ||
                    (haveAuthKey && haveMobileProvision && haveSignCertificate))) {
                result.setError("if Mobileprovision is set or AuthKey is set or SignCertificate is set the three fields are mandatory for Request ID "+form.getRequestId());
                result.setSuccess(false);
                return result;
            }

            AppleSign appleSign = publishRequest.getAppleSign();
            if (!appleSign.getAlias().equals(SignType.CUSTOM)) {
                // and AppleSign is duplicated when the iOS sign is edited. Is not possible to modify the wallet sign.
                appleSign = new AppleSign();
                appleSign.setAlias(SignType.CUSTOM);
            }

            if (haveNewMobileProvision) {
                form.setAppleSignProvisionUrl(uploadResource(form.getAppleSignMobileProvision(), childUppId).toUri().toString());
            }

            if (haveNewSignCertificate) {
                form.setAppleSignCertificateUrl(uploadResource(form.getAppleSignCertificate(), childUppId).toUri().toString());
            }

            appleSign.setUsername(form.getAppleUsername());
            appleSign.setPassword(SecurityPassword.encrypt(form.getApplePassword()));
            appleSign.setTeamId(form.getAppleTeamId());
            appleSign.setTeamName(form.getAppleTeamName());
            appleSign.setCompanyName(form.getAppleCompanyName());
            appleSign.setApplicationPassword(form.getAppleApplicationPassword());
            appleSign.setCertificateUrl(form.getAppleSignCertificateUrl());
            appleSign.setCertificatePsw(form.getAppleSignCertificatePass() == null ? null : SecurityPassword.encrypt(form.getAppleSignCertificatePass()));
            appleSign.setProvisionUrl(form.getAppleSignProvisionUrl());
            appleSign.setAuthKeyId(form.getApplePushAuthKeyId());
            appleSign.setAuthKeyContent(form.getApplePushAuthKeyContent());
            appleSign.setEnvironment(form.isAppleSignCertDevelopment() ? AppleSignCertType.DEVELOPMENT : AppleSignCertType.PRODUCTION);
            AppleSign appleSignSave =  appleSignRepository.save(appleSign);
            publishRequest.setAppleSign(appleSignSave);
        }

        if (form.isEditGooglePlayAccount()) {
            GooglePlaySign googlePlaySign = publishRequest.getGooglePlaySign();
            // only allow to save if is type CUSTOM
            if (googlePlaySign.getAlias() == SignType.CUSTOM) {
                googlePlaySign.setSignatureKeyAlias(form.getGoogleSignKeyAlias());
                googlePlaySign.setSignaturePasswordAlias(form.getGoogleSignPasswordAlias());
                googlePlaySign.setSignaturePasswordStore(form.getGoogleSignPasswordStore());
                googlePlaySign.setSignatureStoreUri(form.getGoogleSignStoreUri());
                googlePlaySign.setTeamName(form.getGoogleTeamName());
                googlePlaySign.setEmail(form.getGoogleEmail());
                googlePlaySignRepository.save(googlePlaySign);
            }
        }

        result.setSuccess(true);
        return result;
    }

    /**
     * Stored the specific iOS status at Database
     *
     * @param requestId Market Request ID to change the iOS status
     * @param status New StatusPublishRequest to be stored
     */
    @Transactional
    public void changeIosStatus(int requestId, StatusPublishRequest status) {
        PublishRequest publishRequest = marketRequestRepository.findOne(requestId);
        publishRequest.setIosStatus(status);
        marketRequestRepository.save(publishRequest);
    }

    /**
     * Stored iOS Market Url at Database
     *
     * @param bundleId Bundle identifier at Itunes Connect
     * @param marketUrl Market Url to stored
     */
    @Transactional
    public void setIosMarketUrl(String bundleId, String marketUrl) {
        ChildUppOS childUppOS = childUppOsRepository.findOne(where(filterByBundleId(bundleId)));
        childUppOS.setMarketUrl(marketUrl);
        childUppOsRepository.save(childUppOS);
    }

    /**
     * Stored IPA creation or publish Error Message received from the server
     *
     * @param requestId Market Request ID
     * @param errorMsg Error message to stored
     */
    @Transactional
    public void setIpaErrorMessage(int requestId, String errorMsg) {
        PublishRequest publishRequest = marketRequestRepository.findOne(requestId);
        // TODO: and Android? right now Android is more safe...
        publishRequest.setErrorMessage(errorMsg);
        marketRequestRepository.save(publishRequest);
    }

    /**
     * Logic Delete for specific Publish Request
     *
     * @param requestId Market Request ID
     */
    @Transactional
    public void deletePublication(int requestId) {
        PublishRequest publishRequest = marketRequestRepository.findOne(requestId);
        publishRequest.setDeleteDate(LocalDate.now().toDate());
        // dont mind about the status
        marketRequestRepository.save(publishRequest);
    }

    /**
     * Delete screenshot for specific Request ID, position and device
     *
     * @param requestId Publish Request ID
     * @param position Position of the screenshot (Number between 1 and 5)
     * @param device Screenshot Device Type (iphone6plus, ipadPro...)
     */
    @Transactional
    public void deleteScreenshot(int requestId, int position, ScreenshotDeviceType device) {
        Screenshot screenshot = screenshotRepository.findOne(matchesCriteria(requestId, position, device));
        if (screenshot != null) screenshotRepository.delete(screenshot.getId());
    }

    /**
     * Upload new icon to public s3 bucket
     *
     * @param requestId Publish Request ID to set the new url icon
     * @param icon MultipartFile with the icon information
     * @return String with new url icon, or null if something is wrong
     * @throws Exception if something wrong happens (uploading the file or saving in the repository)
     */
    @Transactional
    public String uploadIcon(int requestId, MultipartFile icon) {
        PublishRequest publishRequest = marketRequestRepository.findOne(requestId);
        String url = uploadResource(icon, publishRequest.getChildUpp().getId()).toURL().toString();
        publishRequest.setIcon(url);
        queueSender.sendMessage(new NewPWAEvent(URI.create(url), requestId));
        return url;
    }

    /**
     * Upload screenshot to public s3 bucket.
     *
     * @param screenshot Screenshot information to be upload
     * @return New public url from amazon s3 with the screenshot
     * @throws Exception if something wrong happens (uploading the file or saving in the repository)
     */
    @Transactional
    public String uploadScreenshot(ScreenshotForm screenshot) {
        PublishRequest publishRequest = marketRequestRepository.findOne(screenshot.getRequestId());
        String url = uploadResource(screenshot.getFile(), publishRequest.getChildUpp().getId()).toURL().toString();
        storeScreenshot(url, publishRequest, screenshot.getDevice(), screenshot.getPosition());
        return url;
    }

    @Transactional
    public PublishRequestResponse createIosApp(PublishRequestForm request) {
        PublishRequestResponse result = this.savePublication(request);

        if (!result.isSuccess()) {
            return result;
        }

        if (request.getIosStatus() == StatusPublishRequest.NONE) {
            result.setError("We cant publish IOS in status NONE for childupp id: " + request.getApp());
            result.setSuccess(false);
            return result;
        }
        // flag to control if is necessary to build the IPA or not
        if (request.isGenerateIpaBuild()) {
            logger.info("Setting IN_PROGRESS status for ChildUpp ID " + request.getApp());
            creationStatusUtil.updateChildUppStatusRequest(request.getApp(), OperatingSystemType.IOS, StatusRequest.IN_PROGRESS);

            CreationIpaMessage message = builderMessageUtil.buildIpaMessage(request);
            queueSender.sendMessage(message);
        } else {
            logger.info("Is not necessary to generate an IPA build for: " + request.getRequestId() +
                    ", so the message is send to ItunesPublish queue directly");
            ItunesMessageRequest message = builderMessageUtil.buildItunesPublishMessage(request);
            queueSender.sendMessage(message);
        }

        return result;
    }

    @Transactional
    public PublishRequestResponse createAndroidApp(PublishRequestForm request) {
        PublishRequestResponse result = this.savePublication(request);

        if (!result.isSuccess()) {
            return result;
        }
        if (request.getAndroidStatus() == StatusPublishRequest.NONE) {
            result.setError("We cant publish Android in status NONE for childupp id: " + request.getApp());
            result.setSuccess(false);
            return result;
        }

        logger.info("Setting IN_PROGRESS status for ChildUpp ID " + request.getApp());
        creationStatusUtil.updateChildUppStatusRequest(request.getApp(), OperatingSystemType.ANDROID, StatusRequest.IN_PROGRESS);
        CreationApkMessage message = builderMessageUtil.buildApkMessage(request);
        queueSender.sendMessage(message);

        return result;
    }

    /**
     * Notify the creator that his/her app has been published on the Apple Store or Google Play
     *
     * @param form Form with all the information
     * @param os Operative System published
     */
    private void notifyCreatorAppPublished(PublishRequestForm form, String os) {
        ChildUpp childUpp = childUppRepository.findOne(form.getApp());

        String marketUrl = form.getIosMarketUrl();

        if (os.equalsIgnoreCase("android")) marketUrl = form.getAndroidMarketUrl();

        AppPublication notification = new AppPublication()
                .setAppName(childUpp.getName())
                .setMarketUrl(marketUrl)
                .setUserLang(childUpp.getOwner().getLang())
                .setOs(os)
                .setUserName(childUpp.getOwner().getName())
                .setUserMail(childUpp.getOwner().getEmail())
                .setOrganizationName(childUpp.getOwner().getAccount().getOrganization().getName());

        publishRequestEmail.notifyCreatorAppPublished(notification);
    }

    /**
     * Upload new file to a preconfigured S3 bucket
     *
     * @param file MultipartFile to be uploaded not empty
     * @return childUppId int childupp id mandatory
     * @return the new S3Path
     * @throws IllegalStateException in file case error
     */
    private S3Path uploadResource(MultipartFile file, int childUppId) {

        try {
            byte[] bytes = file.getBytes();
            Path path = Files.createTempFile("file_tmp_", file.getOriginalFilename());
            Files.write(path, bytes);
            return awsS3Service.uploadResource(path, childUppId, file.getOriginalFilename());
        } catch (IOException e) {
            throw new IllegalStateException("Error trying to upload the file: " + file, e);
        }
    }

    /**
     * Stored new screenshot at DataBase
     *
     * @param url S3 Url with the screenshots
     * @param publishRequest Publish Request binding at this screenshots
     * @param deviceType Device type of the screenshot to be stores
     * @param position Position of the screenshots at Itunes Connect Dashboard
     */
    private void storeScreenshot(String url, PublishRequest publishRequest, ScreenshotDeviceType deviceType, int position) {
        logger.info("Stored Screenshots " + url + " for device " + deviceType + " at position " + position);
        Screenshot screenshot = new Screenshot();
        screenshot.setRequestId(publishRequest);
        screenshot.setUrl(url);
        screenshot.setDeviceType(deviceType);
        screenshot.setPosition(position);
        screenshotRepository.save(screenshot);
    }

}
