package com.upplication.crm.market.requests.domain;


import java.io.Serializable;

public class ItunesMessageResponse implements Serializable {

    private int id;
    private boolean success;
    private String errorMsg;
    private String stepError;
    private String marketUrl;
    private String bundleId;

    public ItunesMessageResponse() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getStepError() {
        return stepError;
    }

    public void setStepError(String stepError) {
        this.stepError = stepError;
    }

    public String getMarketUrl() {
        return marketUrl;
    }

    public void setMarketUrl(String marketUrl) {
        this.marketUrl = marketUrl;
    }

    public String getBundleId() {
        return bundleId;
    }

    public void setBundleId(String bundleId) {
        this.bundleId = bundleId;
    }

    @Override
    public String toString() {
        return "PublishRequestResponse{" +
                "id='" + id + '\'' +
                ", success='" + success + '\'' +
                ", stepError='" + stepError + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", marketUrl='" + marketUrl + '\'' +
                ", bundleId='" + bundleId + '\'' +
                '}';
    }

}
