package com.upplication.crm.market.requests.queue;

import com.upplication.crm.market.requests.domain.CreationApkMessage;
import com.upplication.crm.market.requests.domain.CreationIpaMessage;
import com.upplication.crm.market.requests.domain.NewPWAEvent;
import com.upplication.crm.market.requests.domain.ItunesMessageRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class QueueSender {

    private static final Logger logger = LoggerFactory.getLogger(QueueSender.class);

    @Autowired
    private Environment environment;

    private final RabbitTemplate rabbitTemplate;

    public QueueSender(final RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    /**
     * Send message to the Creation IPA Queue
     *
     * Name of the queue: 'creationIpa'
     *
     * @param message Object with all the mandatory fields for the Creation IPA Worker
     */
    public void sendMessage(CreationIpaMessage message) {
        logger.info("INFO Sending Creation IPA Message to Creation IPA Queue to process in a Worker with Bundle ID: " + message.getBundleId());
        rabbitTemplate.convertAndSend(environment.getProperty("rabbitmq.ipa.creation.exchange"), environment.getProperty("rabbitmq.ipa.creation.routingKey"), message);
    }

    /**
     * Send message to the Itunes Publish queue
     *
     * Name of the queue: 'itunesPublish'
     *
     * @param request Object with all the Market Publish Request information
     */
    public void sendMessage(ItunesMessageRequest request) {
        logger.info("INFO Sending Request to ItunesPublish Queue to process in a Worker with Request Id: " + request.getRequestId());
        rabbitTemplate.convertAndSend(environment.getProperty("rabbitmq.itunes.publish.exchange"), environment.getProperty("rabbitmq.itunes.publish.routingKey"), request);
    }

    /**
     * Send message to the Creation APK Queue
     *
     * Name of the queue: 'creationApk'
     *
     * @param message Object with all the mandatory fields for the Creation APK Worker
     */
    public void sendMessage(CreationApkMessage message) {
        logger.info("INFO Sending Creation APK Message to Creation APK Queue to process in a Worker with Bundle ID: " + message.getBundleId());
        rabbitTemplate.convertAndSend(environment.getProperty("rabbitmq.apk.creation.exchange"), environment.getProperty("rabbitmq.apk.creation.routingKey"), message);
    }

    /**
     * Send message to the PWA Icon Queue
     *
     * Name of the queue: 'pwaIcons'
     *
     * @param message Object with all the mandatory fields for the Creation APK Worker
     */
    public void sendMessage(NewPWAEvent message) {
        logger.info("INFO Sending PWA icon to 'pwaIcons' Queue to process");
        rabbitTemplate.convertAndSend(environment.getProperty("rabbitmq.pwa.icons.exchange"), environment.getProperty("rabbitmq.pwa.icons.routingKey"), message);
    }
}
