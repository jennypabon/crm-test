package com.upplication.crm.stats;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class StatsController {

    private static final String APLICATECA_INDEX = "stats/index";

    @Autowired
    private StatsManager statsManager;

    @RequestMapping(value = "stats")
    public String sales(Model model,
                        @RequestParam(value = "start", required = false)  @DateTimeFormat(pattern = "yyyy-MM-dd") java.util.Date start,
                        @RequestParam(value = "end", required = false)  @DateTimeFormat(pattern = "yyyy-MM-dd") java.util.Date end,
                        @RequestParam(value = "filterType", required = false, defaultValue = "ALL")  FilterType filterType) {

        model.addAttribute("start", start);
        model.addAttribute("end", end);
        model.addAttribute("filterType", filterType);

        List<StatsSales> news = statsManager.getSubscriptions(start, end, filterType);

        List<StatsSales> downs = statsManager.getUnsubscriptions(start, end, filterType);

        model.addAttribute("news", news);
        model.addAttribute("downs", downs);

        return APLICATECA_INDEX;
    }

    @PostMapping(value = "stats/export.csv")
    @ResponseBody
    public String statsExportCSV(@Valid @ModelAttribute StatAdminForm adminForm, HttpServletResponse response) {
        response.setHeader("Content-Disposition", "attachment; filename=Stats_" + LocalDate.now().toString() + ".csv");
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(StatExportHelper
            .statsSalesToString(statsManager.getSubscriptions(adminForm.getStart(), adminForm.getEnd(), adminForm.getFilterType()), true));
        stringBuilder.append(StatExportHelper
            .statsSalesToString(statsManager.getUnsubscriptions(adminForm.getStart(), adminForm.getEnd(), adminForm.getFilterType()), false));

        return stringBuilder.toString();
    }


}
