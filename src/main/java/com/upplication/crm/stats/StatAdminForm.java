package com.upplication.crm.stats;

import com.upplication.crm.common.forms.SearchAdminForm;

public class StatAdminForm extends SearchAdminForm {

    private FilterType filterType;

    public FilterType getFilterType() {
        return filterType;
    }

    public void setFilterType(FilterType filterType) {
        this.filterType = filterType;
    }
}
