package com.upplication.crm.stats;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class StatExportHelper {

    public static String statsSalesToString(List<StatsSales> statsSales, boolean news){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(getCSVHeader(statsSales.get(0), news));
        for (StatsSales result : statsSales) {
            stringBuilder.append(result.getPricingName() + ", ");
            result.getValues().values().stream().forEach(value ->{
                stringBuilder.append(value + ", ");
            });
            stringBuilder.append(result.getTotal() + "\n");
        }

        return stringBuilder.toString();
    }

    private static String getCSVHeader(StatsSales statsSales, boolean news){
        StringBuilder header = new StringBuilder();
        String type = news ? "News" : "Down";
        header.append("Pricing plan for " + type + ", ");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        statsSales.getValues().keySet().stream().forEach(localDate -> {
            header.append(localDate.format(formatter) +", ");
        });

        header.append("Total\n");
        return header.toString();
    }

}
