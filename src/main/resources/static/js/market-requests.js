(function (window, $) {
    'use strict';

    $(function () {

        $('.app-main-form').on('submit', function () {
            return false;
        });

        $(".save-btn").click(function(e) {
            e.preventDefault();
            var $form = $(this).parents("form"),
                requestId = $form.find("input[name='requestId']").val();

            if (validateFields($form)) {
                var data = new FormData($form[0]);

                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: $(this).attr("value"),
                    data: data,
                    //http://api.jquery.com/jQuery.ajax/
                    //https://developer.mozilla.org/en-US/docs/Web/API/FormData/Using_FormData_Objects
                    processData: false, //prevent jQuery from automatically transforming the data into a query string
                    contentType: false,
                    cache: false,
                    timeout: 600000,
                    success: function (result) {
                        if (result.success) {
                            showSuccessMessage(requestId);
                        } else {
                            showErrorMessage(requestId, result.error);
                        }
                    },
                    error: function (e) {
                        showErrorMessage(requestId, "Error: something wrong happens. " + e);
                    }
                });
            } else {
                showErrorMessage(requestId, "Review mandatory fields");
            }
        });

        $(".upload-icon-btn").click(function(e) {
            e.preventDefault();
            var $form = $(this).parents("form"),
                requestId = $form.find("input[name='requestId']").val(),
                newIcon = $('#upload-icon-file_' + requestId),
                data = new FormData();

            if (!newIcon[0].files[0]){
                // we dont have screenshot
                showIpaErrorMessage(requestId, "Select a icon to upload before clicking upload!");
                return;
            }

            data.append("requestId", requestId);
            data.append("icon", newIcon[0].files[0], newIcon[0].files[0].name);

            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: $(this).attr("value"),
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
                success: function (data) {
                    showSuccessMessage(data.id);
                    var $iconInput = $('#icon-file_' + data.id);
                    $iconInput.val(data.url);
                    var $iconView = $('#icon-view-file_' + data.id);
                    $iconView.attr("href", data.url);
                },
                error: function (e) {
                    showIpaErrorMessage(requestId, "Something wrong uploading icon");
                }
            });
        });

        $(".upload-screenshot-btn").click(function(e) {
            e.preventDefault();
            var requestId = this.getAttribute('data-id'),
                position = this.getAttribute('data-position'),
                deviceType = this.getAttribute('data-device'),
                newFile = $('#upload-input-file_' + requestId + '_' + position + '_' +deviceType),
                data = new FormData();

            if (!newFile[0].files[0]){
                // we dont have screenshot
                showIpaErrorMessage(requestId, "Select a screenshot to upload before clicking upload!");
                return;
            }

            data.append("requestId", requestId);
            data.append("position", position);
            data.append("device", deviceType);
            data.append("file", newFile[0].files[0], newFile[0].files[0].name);

            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: $(this).attr("value"),
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
                success: function (data) {
                    showSuccessMessage(data.id);
                    var $screenView = $('#view-screenshot_'+data.id+'_'+data.position+'_'+data.device);
                    $screenView.attr("href", data.url);

                    var $viewElement = $('#view-div-btn_'+data.id+'_'+data.position+'_'+data.device);
                    $viewElement.show();
                },
                error: function (e) {
                    showIpaErrorMessage(requestId, "Something wrong uploading screenshot");
                }
            });

        });

        $(".delete-btn").click(function(e) {
            e.preventDefault();
            var $form = $(this).parents("form"),
                requestId = $form.find("input[name='requestId']").val(),
                $element = $('#delete-date_' + requestId);
            $.post($(this).attr("value"), {requestId: requestId}, function() {
                $element.html(todayFormatDate());
                disableAllBtns(requestId);
            }).fail(function (xhr, status, error) {
                showErrorMessage(requestId, "Something wrong deleting PR. " + error);
            });
        });

        $(".delete-screenshot-btn").click(function(e) {
            e.preventDefault();
            var requestId = this.getAttribute('data-requestId'),
                position = this.getAttribute('data-position'),
                device = this.getAttribute('data-device');
            $.post($(this).attr("value"), {requestId: requestId, position: position, device: device}, function(data) {
                var $viewElement = $('#view-div-btn_'+data.id+'_'+data.position+'_'+data.device);
                $viewElement.hide();
            }).fail(function (xhr, status, error) {
                showIpaErrorMessage(requestId, "Error deleting screenshot. " + error);
            });
        });

        $(".publish-ios-btn").click(function(e) {
            e.preventDefault();
            var $form = $(this).parents("form"),
                requestId = $form.find("input[name='requestId']").val(),
                $iosMsg = $('#publish-ios-msg_' + requestId),
                $element = $('#ios-status_' + requestId);
            if (validateFields($form)) {
                $.post($(this).attr("value"), $form.serialize(), function (result) {

                    if (result.success) {
                        $element.val("IN_PROGRESS");
                        $iosMsg.addClass("alert-succes").removeClass("alert-danger");
                        $iosMsg.html('iOS Publish IN PROGRESS');
                    } else {
                        $iosMsg.addClass("alert-danger").removeClass("alert-success");
                        $iosMsg.html(result.error);
                    }
                    $iosMsg.show();
                }).fail(function (xhr, status, error) {
                    showIpaErrorMessage(requestId, "Error Creating/Publishing iOS");
                    $iosMsg.addClass("alert-danger").removeClass("alert-success");
                    $iosMsg.html('Something wrong creating/publishing iOS. ' + error);
                    $iosMsg.show();
                });
            } else {
                $iosMsg.html('<span class="alert alert-danger">Review mandatory fields</span>');
                $iosMsg.show();
            }
        });

        $(".publish-android-btn").click(function(e) {
            e.preventDefault();
            var $form = $(this).parents("form"),
                requestId = $form.find("input[name='requestId']").val(),
                $androidMsg = $('#publish-android-msg_' + requestId),
                $element = $('#android-status_' + requestId);
            if (validateFields($form)) {
                $.post($(this).attr("value"), $form.serialize(), function (result) {
                    if (result.success) {
                        $element.val("IN_PROGRESS");
                        $androidMsg.addClass("alert-success").removeClass("alert-danger")
                        $androidMsg.html('Android Publish IN PROGRESS');
                    } else {
                        $androidMsg.addClass("alert-danger").removeClass("alert-success")
                        $androidMsg.html(result.error);
                    }
                    $androidMsg.show();
                }).fail(function (xhr, status, error) {
                    $androidMsg.addClass("alert-danger").removeClass("alert-success")
                    $androidMsg.html('Something wrong publishing Android. ' + error);
                    $androidMsg.show();
                });
            } else {
                $androidMsg.addClass("alert-danger").removeClass("alert-success")
                $androidMsg.html('Review mandatory fields');
                $androidMsg.show();
            }
        });

        $(".show-default-values").click(function(e) {
            e.preventDefault();
            var id = this.getAttribute('data-id');
            var $defaults = $('#default-values-form_' + id);
            $defaults.show();
        });

        $(".edit-apple-tablet-screenshots").change(function(e) {
            console.log("changing data");
            e.preventDefault();
            var id = this.getAttribute('data-id');
            var $iosSign = $('#apple-tablet-screenshots-values-form_' + id);
            if (this.checked) {
                $iosSign.show();
            } else {
                $iosSign.hide();
            }
        });

        $(".edit-apple-mobile-screenshots").change(function(e) {
            console.log("changing data");
            e.preventDefault();
            var id = this.getAttribute('data-id');
            var $iosSign = $('#apple-mobile-screenshots-values-form_' + id);
            if (this.checked) {
                $iosSign.show();
            } else {
                $iosSign.hide();
            }
        });

        $(".edit-apple-developer-check").change(function(e) {
            console.log("changing data");
            e.preventDefault();
            var id = this.getAttribute('data-id');
            var $iosSign = $('#apple-developer-values-form_' + id);
            if (this.checked) {
                $iosSign.show();
            } else {
                $iosSign.hide();
            }
        });

        $(".edit-google-play-check").change(function(e) {
            console.log("changing data");
            e.preventDefault();
            var id = this.getAttribute('data-id');
            var $iosSign = $('#google-play-values-form_' + id);
            if (this.checked) {
                $iosSign.show();
            } else {
                $iosSign.hide();
            }
        });

        /**
         * Check all fields of the Publish Request
         * @param $form Form parent with all the information content
         * @return {boolean} true is all fields are valid, false otherwise
         */
        function validateFields($form) {
            var valid = true,
                $description = $form.find("input[name='description']"),
                $keywords = $form.find("input[name='keywords']"),
                $content = $form.find("input[name='content']"),
                $iosMarketUrl = $form.find("input[name='iosMarketUrl']"),
                $androidMarketUrl = $form.find("input[name='androidMarketUrl']"),
                $supportUrl = $form.find("input[name='supportUrl']");

            // contact fields
            var $copyright = $form.find("input[name='copyright']"),
                $contactName = $form.find("input[name='contactName']"),
                $contactSurname = $form.find("input[name='contactSurname']"),
                $contactPhone = $form.find("input[name='contactPhone']"),
                $contactEmail = $form.find("input[name='contactEmail']");

            // remove previous errors or info message
            $description.removeClass('warning');
            $keywords.removeClass('warning');
            $content.removeClass('warning');
            $iosMarketUrl.removeClass('warning');
            $androidMarketUrl.removeClass('warning');
            $supportUrl.removeClass('warning');
            $copyright.removeClass('warning');
            $contactName.removeClass('warning');
            $contactSurname.removeClass('warning');
            $contactPhone.removeClass('warning');
            $contactEmail.removeClass('warning');

            var requestId = $form.find("input[name='requestId']").val();
            var $saveSuccess = $('#save-success_' + requestId);
            $saveSuccess.hide();
            var $iosMsg = $('#publish-ios-msg_' + requestId);
            $iosMsg.hide();
            var $androidMsg = $('#publish-android-msg_' + requestId);
            $androidMsg.hide();

            if ($description.val().length < 10) {
                $description.addClass('warning');
                valid = false;
            }

            if ($keywords.val().length < 3) {
                $keywords.addClass('warning');
                valid = false;
            }

            if ($content.val().length < 1) {
                $content.addClass('warning');
                valid = false;
            }

            // Urls validations
            if ($iosMarketUrl.val().length > 0 && !isUrlValid($iosMarketUrl.val())) {
                $iosMarketUrl.addClass('warning');
                valid = false;
            } else if ($iosMarketUrl.val().length <=0) {
                // mandatory with status PUBLISHED
                var $iosStatus = $('#ios-status_' + requestId);
                if ($iosStatus.val().localeCompare("PUBLISHED") == 0) {
                    $iosMarketUrl.addClass('warning');
                    valid = false;
                }
            }

            if ($androidMarketUrl.val().length > 0 && !isUrlValid($androidMarketUrl.val())) {
                $androidMarketUrl.addClass('warning');
                valid = false;
            } else if ($androidMarketUrl.val().length <= 0) {
                // mandatory with status PUBLISHED
                var $androidStatus = $('#android-status_' + requestId);
                if ($androidStatus.val().localeCompare("PUBLISHED") == 0) {
                    $androidMarketUrl.addClass('warning');
                    valid = false;
                }
            }

            // mandatory
            if ($supportUrl.val().length <= 0 || !isUrlValid($supportUrl.val())) {
                $supportUrl.addClass('warning');
                valid = false;
            }

            // contact fields
            if ($copyright.val().length < 1) {
                $copyright.addClass('warning');
                valid = false;
            }

            if ($contactName.val().length < 1) {
                $contactName.addClass('warning');
                valid = false;
            }

            if ($contactSurname.val().length < 1) {
                $contactSurname.addClass('warning');
                valid = false;
            }

            if ($contactPhone.val().length < 1) {
                $contactPhone.addClass('warning');
                valid = false;
            }

            if ($contactEmail.val().length < 1) {
                $contactEmail.addClass('warning');
                valid = false;
            }

            return valid;
        }

        /**
         * Regex to check if is a valid url format
         * @param url URL to check
         * @return {boolean} true if is a valid url, false otherwise
         */
        function isUrlValid(url) {
            return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
        }

        /**
         * Construct the today date with specific format -> yyyy+'/'+mm+'/'+dd+' '+hh+':'+mn+':'+ss
         * @return {string} Parsed date
         */
        function todayFormatDate() {
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth()+1; //January is 0!

            var yyyy = today.getFullYear();
            if(dd<10){
                dd='0'+dd;
            }
            if(mm<10){
                mm='0'+mm;
            }

            var hh = today.getHours();
            var mn = today.getMinutes();
            var ss = today.getSeconds();
            return yyyy+'/'+mm+'/'+dd+' '+hh+':'+mn+':'+ss;
        }

        /**
         * Disable all buttons at the subform with the Publish Request identification
         * @param id Publish Request identification
         */
        function disableAllBtns(id) {
            var $btnSave = $('#btn-save_' + id),
                $btnPublishAndroid = $('#btn-publish-android_' + id),
                $btnPublishIos = $('#btn-publish-ios_' + id),
                $btnDelete = $('#btn-delete_' + id);

            $btnSave.prop('disabled', true);
            $btnPublishAndroid.prop('disabled', true);
            $btnPublishIos.prop('disabled', true);
            $btnDelete.prop('disabled', true);
        }

        /**
         * Show success label information
         * @param id Publish Request identifier label
         */
        function showSuccessMessage(id) {
            var $successElement = $('#save-success_' + id);
            $successElement.show();
        }

        /**
         * Show success label information
         * @param id Publish Request identifier label
         */
        function showErrorMessage(id, text) {
            var $errorElement = $('#save-error_' + id);
            $errorElement.show();
            $errorElement.html(text);
        }

        /**
         * Set specific IOS error message at the error label
         * @param id Publish Request identifier label
         * @param message Message to show at the label
         */
        function showIpaErrorMessage(id, message) {
            var $errorMsg = $('#error-message_' + id);
            $errorMsg.html(message);
        }

    });

}(window, jQuery));